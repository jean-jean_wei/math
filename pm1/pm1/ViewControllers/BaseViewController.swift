//
//  BaseViewController.swift
//  pm1
//
//  Created by Jean-Jean Wei on 2016-11-25.
//  Copyright © 2016 Jean-Jean Wei. All rights reserved.
//

import UIKit

class BaseViewController: ViewController, UIPopoverPresentationControllerDelegate
{
    @IBOutlet var a1: UIButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        view.backgroundColor = UIColor.green
        
        // assign Keypad event handler callback function
        Keypad.instance.onTapped = { result in
            if (result == Keypad.instance.CLEAR_SIGN)
            {
                self.a1.setTitle("", for: UIControlState.normal)
            }
            else if (result == Keypad.instance.X_SIGN)
            {
                // get a reference to the view controller for the popover
                let popController = Keypad.instance.getPopController();
                popController.dismiss(animated: true, completion: nil)
            }
            else
            {
                self.a1.setTitle(String(result), for: UIControlState.normal)
            }
        }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonTapped(_ sender: UIButton)
    {
        // get a reference to the view controller for the popover
        let popController = Keypad.instance.getPopController();
        
        // set the presentation style
        popController.modalPresentationStyle = UIModalPresentationStyle.popover
        
        // set up the popover presentation controller
        popController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.any
        popController.popoverPresentationController?.delegate = self
        popController.popoverPresentationController?.sourceView = sender //as! UIView // button
        popController.popoverPresentationController?.sourceRect = sender.bounds
        
        // present the popover
        self.present(popController, animated: true, completion: nil)
    }

    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle
    {
        // return UIModalPresentationStyle.FullScreen
        // Force popover style
        return UIModalPresentationStyle.none
    }
    
}
